import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import HomeView from './components/HomeView';
import Repositories from './components/Repositories';
import NotFoundView from './components/NotFoundView';
import Plugins from './components/Plugins';
import { environment } from './environments/environment';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {
        email: props.email,
        username: props.username,
        first_name: props.first_name,
        last_name: props.last_name,
      }
    }
  }

  componentDidMount() {
    fetch( environment.backend_url + '/api/users/me/', { credentials: 'include' })
      .then(
        res => res.json())
      .then(
        response => {
          this.setState({
            user : {
              email: response.email,
              username: response.username,
              first_name: response.first_name,
              last_name: response.last_name,              
            }
          })
        },
        (error) => {
          console.log("ERROR IS ", error);
        }
      )
  }

  render() {
    const { user } = this.state;

    return (
      <Switch>
        <Route path="/repo/:id">
          {
            (props) => <Plugins user={this.state.user} {...props} />
          }
        </Route>
        <Route path="/repositories">
          {
            user.email == undefined ? <Redirect to="/" /> : <Repositories user={this.state.user} />
          }
        </Route>
        <Route path="/">
          {
            user.email == undefined ? <HomeView /> : <Redirect to="/repositories" />
          }
        </Route>
        <Route component={NotFoundView} />
      </Switch>
    )
  }
};

export default App;

import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import { IconButton } from '@material-ui/core';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { environment } from '../environments/environment';
import NotFoundView from './NotFoundView';

class Plugins extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            user: this.props.user,
            repoid: this.props.match.params.id,
            validRepoid: false,
            repo: {}
        }
    }

    componentDidMount(){
        fetch( environment.backend_url + '/api/repos/' + this.state.repoid + '/', { credentials: 'include' })
        .then(
            res => {
                if(res.status==200)
                    return res.json();
                else return {id: 0};
            })
        .then(
            response => {
                if(response.id !== 0){
                    this.setState({
                        validRepoid: true,
                        repo: response
                    })
                } else {
                    this.setState({
                        validRepoid: false,
                        repo: {}
                    })
                }
            },
            (error) => console.log("error is: ", error)
        );
    }
    
    render(){

        if(this.state.user.email == undefined){
            return(
                <Redirect to="/" />
            )
        }

        if(this.state.validRepoid == false){
            return <NotFoundView />
        }

        return(
            <div>
                <Link to="/repositories">
                    <IconButton>
                        <KeyboardBackspaceIcon />
                    </IconButton>
                </Link>
                <div style={{marginLeft: "10%"}}>
                    <h2>{this.state.repo.full_name}</h2>
                    <p>
                        Display all plugins here
                    </p>
                </div>
            </div>
        )
    }
}

export default Plugins;
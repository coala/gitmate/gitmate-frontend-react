import React from 'react';
import { environment } from '../environments/environment';
import Button from '@material-ui/core/Button';

const HomeView = () => {

    const LoginGitHub = () => {
        window.location.href = environment.backend_url + '/auth/login/github/';
    }

    const LoginGitLab = () => {
        window.location.href = environment.backend_url + '/auth/login/gitlab/';
    }

    return (
        <div>
            <h1>
                GitMate
            </h1>
            <Button variant="contained" color="primary" onClick={LoginGitHub}>
                Login With GitHub
            </Button>
            <Button variant="contained" color="primary" onClick={LoginGitLab}>
                Login With GitLab
            </Button>
        </div>
    )
}

export default HomeView;

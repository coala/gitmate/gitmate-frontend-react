import React from 'react';
import { environment } from '../environments/environment';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Switch from '@material-ui/core/Switch';
import SettingsIcon from '@material-ui/icons/Settings';
import update from 'immutability-helper';
import { IconButton } from '@material-ui/core';
import { Link } from 'react-router-dom';

const Logout = () => {
    window.location.href = environment.backend_url + '/logout';
}

class Repositories extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            repos: []
        }
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        if(this.state.repos !== null){
        fetch( environment.backend_url + '/api/repos/?cached=1', { credentials: 'include' })
          .then(
              res => res.json())
          .then(
              response => {
                  this.setState({
                      repos: response
                  })
              },
          (error) => {
              console.log("error is: ", error);
          })
        }
    }

    handleChange(event,repo){
        if(!repo.active){
            fetch( environment.backend_url + '/api/repos/' + repo.id + '/' , {
                method: 'PATCH',
                body: JSON.stringify({active: 'true'}),
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                },
            })
            .then(res => res.json())
            .then(response => {
                const index = this.state.repos.findIndex((repo) => repo.id == response.id);
                this.setState(update(
                    this.state, {
                        repos : {
                            [index]: {
                                $set: response
                            }
                        }
                    })
                );
              },
            (error) => console.log("error is: ", error)
            );
        } else {
            fetch( environment.backend_url + '/api/repos/' + repo.id + '/', {
                method: 'PATCH',
                body: JSON.stringify({active: 'false'}),
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                },                
            })
            .then(res => res.json())
            .then(response => {
                const index = this.state.repos.findIndex((repo) => repo.id == response.id);
                this.setState(update(
                    this.state, {
                        repos : {
                            [index]: {
                                $set: response
                            }
                        }
                    })
                );
              },
            (error) => console.log("error is: ", error)
            );
        }
    }

    render(){
        const username = this.props.user.username;

        var reposList=[];

        if(this.state.repos !== null){
            reposList = this.state.repos.map((repo,index) => {
                return(
                    <Card style={{display:"flex", justifyContent: "space-between"}}>
                        <CardContent>
                        <Typography variant="h5" component="h2">
                            {repo.full_name.split('/').pop()}
                        </Typography>
                        <Typography color="textSecondary">
                            {repo.provider}/{repo.user}
                        </Typography>
                        </CardContent>
                        <CardActions>
                            { 
                                repo.active ? (
                                    <Link to={"/repo/" + repo.id}>
                                        <IconButton>
                                            <SettingsIcon />
                                        </IconButton>
                                    </Link> 
                                ) : null 
                            }
                            <Switch checked={repo.active} onChange={(event) => this.handleChange(event,repo)} />
                        </CardActions>
                    </Card>
                )
            });
        }

        return (
            <div>
                <Button variant="contained" color="primary" onClick={Logout}>
                    Logout
                </Button>
                <h2>
                    Welcome {username}!
                </h2>
                <h1>
                    Repositories
                </h1>
                <div style={{width:"70%", marginLeft: "15%", paddingBottom:"10vh"}}>
                {
                    reposList.map((repo) => {
                        return(repo)
                    })
                }
                </div>
            </div>
        )
    }
}

export default Repositories;
